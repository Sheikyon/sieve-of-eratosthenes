a = int(input("Enter the starting range: "))                       # Takes an integer as the first argument of the range()
b = int(input("Enter the end range: "))                            # Takes an integer as the second and last argument of the range()

def primesInRange(a,b):
    isPrime = [True] * (b - a + 1)                                 # Partial sieve
    if a < 2: isPrime[:2-a] = [False] * (2 - a)                    # 0 and 1 are not primes
    for p in (2, *range(3, int(b**0.5) + 2,2)):                    # Scan divisors up to √b
        base = (p - a % p) %p + p* (p >= a)                        # First multiple in partial sieve
        isPrime[base::p] = [False] *len(isPrime[base::p])          # Flag non-primes
    return [p for p, prime in enumerate(isPrime, a) if prime]      # Return primes

primes = primesInRange(a, b)

print(*primes)

print(len(primes))