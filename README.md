# sieve-of-eratosthenes

The sieve of Eratosthenes, applied to Python and JavaScript for the computation of prime numbers taking an integer interval.